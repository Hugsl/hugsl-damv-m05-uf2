﻿using System;
using System.Linq;

namespace ExercicisOptimització
{
    public class Program
    {

        
            static void Main()
            {
                Program a = new Program();
                a.Mn();
            }

            public void Mn()
            {
                string a = "0";
                do
                {
                    Mnu();
                    a = Console.ReadLine();
                    call(a);

                } while (a != "e");
            }

            public void Mnu()
            {

                Console.WriteLine("Select 1-4:");
                Console.WriteLine("1 = Isleapyear");
                Console.WriteLine("2 = Min10Val");
                Console.WriteLine("3 = Howmanydays");
                Console.WriteLine("4 = DNI");
                Console.WriteLine();
            }

            public void call(string a)
            {
                switch (a)
                {
                    case "1":
                        IsLeapyear(0);
                        Console.Clear();
                        break;
                    case "2":
                        MinOf10Values();
                        Console.Clear();
                        break;
                    case "3":
                        HowManyDays();
                        Console.Clear();
                        break;
                    case "4":
                        Dni(0);
                        Console.Clear();
                        break;
                    default:
                        Console.Clear();

                        break;
                }
            }

            public bool IsLeapyear(int any)
            {

                Console.Write("Introdueix un any:");

                /*any = Convert.ToInt32(Console.ReadLine());*/
                bool tf;

                if ((any % 4 == 0) && (any % 100 != 0))
                {

                    Console.WriteLine("{0} es de traspas", any);
                    return true;

                }
                else if ((any % 100 == 0) && (any % 400 == 0))
                {

                    Console.WriteLine("{0} es de traspas", any);
                    return true;
                }
                else
                {
                    Console.WriteLine("{0} no es de traspas", any);
                    return false;
                }
            }

            public int MinOf10Values()
            {
                int[] a = new int [10];
                int smoll;
                for (int i = 1; i < a.Length; i++)
                {
                    a[i] = Convert.ToInt32(Console.ReadLine());
                }

                smoll = a.Min();
                return smoll;
            }

            public void HowManyDays()
            {

                int a;
                int[] days2 = new int[] {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                int[] days1 = new int[] {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                a = tof();
                int month = Convert.ToInt32(Console.ReadLine());
                do
                {
                    month = Convert.ToInt32(Console.ReadLine());
                } while (month < 1 || month > 12);

                int final;
                if (a == 1)
                {
                    final = days1[month];
                }
                else
                {
                    final = days2[month];
                }
            }

            public int tof()
            {
                int yas = 0;
                do
                {
                    Console.Write("Introdueix un any:");

                    int any = Convert.ToInt32(Console.ReadLine());

                    if ((any % 4 == 0) && (any % 100 != 0))
                    {

                        Console.WriteLine("{0} es de traspas", any);
                        yas = 1;

                    }
                    else if ((any % 100 == 0) && (any % 400 == 0))
                    {

                        Console.WriteLine("{0} es de traspas", any);
                        yas = 2;
                    }
                    else
                    {
                        Console.WriteLine("{0} no es de traspas", any);
                    }
                } while (yas == 1 || yas == 2);

                return yas;
            }

            public char Dni(int total)
            {
                int mod;
                char result;
                do
                {
                    total = Convert.ToInt32(Console.ReadLine());
                } while (total - Math.Pow(10, 6) < 0 || total - Math.Pow(10, 8) > 0);

                mod = total % 23;
                result = metamorphosis(mod);
                Console.WriteLine(result);
                return result;
            }

            /* public int changes()
             {
                 int a; 
                 int total = 0;
                 for (int  j = 7; 0 <= j; j--)
                 {
                     if (j != 0)
                     {
                         a = Convert.ToInt32(Console.ReadLine()) * (10 ^ j);
                     }
                     else
                     {
                         a = Convert.ToInt32(Console.ReadLine());
                     }
     
                     total = a + total;
                    
                 }
                 return total;
             }
             */

            public char metamorphosis(int mod)
            {
                char a = 'a';
                switch (mod)
                {
                    case 0:
                        a = 'T';
                        break;
                    case 1:
                        a = 'R';
                        break;
                    case 2:
                        a = 'W';
                        break;
                    case 3:
                        a = 'A';
                        break;
                    case 4:
                        a = 'G';
                        break;
                    case 5:
                        a = 'M';
                        break;
                    case 6:
                        a = 'Y';
                        break;
                    case 7:
                        a = 'F';
                        break;
                    case 8:
                        a = 'P';
                        break;
                    case 9:
                        a = 'D';
                        break;
                    case 10:
                        a = 'X';
                        break;
                    case 11:
                        a = 'B';
                        break;
                    case 12:
                        a = 'N';
                        break;
                    case 13:
                        a = 'J';
                        break;
                    case 14:
                        a = 'Z';
                        break;
                    case 15:
                        a = 'S';
                        break;
                    case 16:
                        a = 'Q';
                        break;
                    case 17:
                        a = 'V';
                        break;
                    case 18:
                        a = 'H';
                        break;
                    case 19:
                        a = 'L';
                        break;
                    case 20:
                        a = 'C';
                        break;
                    case 21:
                        a = 'K';
                        break;
                    case 22:
                        a = 'E';
                        break;
                    default:
                        Console.WriteLine("Error");
                        break;
                }

                return a;
            }



        }
    }
