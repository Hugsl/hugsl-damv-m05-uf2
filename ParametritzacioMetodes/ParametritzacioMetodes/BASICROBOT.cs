using System;
using System.ComponentModel.Design;

namespace ParametritzacioMetodes
{/// <summary>
 /// The Basic Robot program is about, like the program name says, control a robot, it's cool 👍  
 /// </summary>
    public class BASICROBOT
    {/// <summary>
     /// Like in the other programs here we have, the Start method is the initialization part of the program
     /// </summary>
        public void Start()
        {   //The robot possition, and speed (X,Y,S) 
            double[] numbers = new double[]{0.00,0.00,1.00};
            //The user command
            string code;

            do
            {
                code=Console.ReadLine()?.ToUpper();
                if (code !="END")
                {
                  numbers = Commands(numbers,code);
                }
                
            } while (code!="END");
            
        }
/// <summary>
/// This is the program's magic, here we use the word the user wrote and we modify the array or print what the user demands
/// </summary>
/// <param name="numbers"> The Robot Position and speed </param>
/// <param name="code">The user commands</param>
/// <returns>Remember! It returns the array!!</returns>
        private double[] Commands(double[] numbers, string code)
        {switch (code)
            {
                case "DALT":
                    numbers[1] += numbers[2];
                    break;
                    
                case "BAIX":
                    numbers[1] -= numbers[2];
                    break;
                    
                case "DRETA":
                    numbers[0] += numbers[2];
                    break;
                    
                case "ESQUERRA":
                    numbers[0] -= numbers[2];
                    break;
                    
                case "ACCELERAR":
                    if (numbers[2]<10)
                    {
                        numbers[2] += 0.50;
                    }
                    else
                    {
                        Console.WriteLine("La velocitat no pot ser més gran que 10");
                    }
                    break;
                    
                case "DISMINUIR":
                    if (numbers[2]>0)
                    {
                        numbers[2] -= 0.50;
                    }
                    else
                    {
                        Console.WriteLine("La velocitat no pot ser menor que 0");
                    }
                    break;
                    
                case "POSICIO":
                    Console.WriteLine($"La posició del robot és ({numbers[0]}  ,   {numbers[1]})");
                    break;
                    
                case "VELOCITAT":
                    Console.WriteLine($"La velocitat actual del robot és de: {numbers[2]}");
                    break;
            }
            return numbers;
        }
    }
}