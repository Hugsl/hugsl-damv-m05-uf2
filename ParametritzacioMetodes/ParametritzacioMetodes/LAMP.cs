using System;

namespace ParametritzacioMetodes
{
    /// <summary>
    /// It's a Lamp, it can be ON or Off , but sometimes is odd.
    /// </summary>
    public class LAMP
    {/// <summary>
     /// Here we declare all the variables and it's the initialization of the program
     /// </summary>
        public void Start()
        {   // this int is to control the light at the beginning, after that is useless 
            int controler=-1;
            // The most inportant var, is the light 👍 
            bool light=false;
            // lights is the controler, if lights is off and light is of that means that the lightbulb is off and you can't turn it off again.
            bool lights;
            // this is the input message that the user can put
            string a;
           do
            { lights = light;
                if (controler==-1)
                {
                   
                    Console.WriteLine("La llum actualment està apagada");
                    controler++;
                }
                a = Console.ReadLine()?.ToUpper();
                if (a!="END")
                {
                    
                   light= Light(a,light);
                }

                mesage(light,lights);
            } while (a!="END");
            
        }
/// <summary>
/// OK, here is where tha magic happens, u need the string you generated before and the light var, and switches the value
/// of light and then it returns it to start to print the message.
/// </summary>
/// <param name="a"> The command u request before</param>
/// <param name="light"> The Light 👍 </param>
/// <returns> REMEMBER IT RETURNS A BOOL !!!</returns>
        public static bool Light(string a,bool light)
        {

            switch (a)
            { case ("TURN ON"):
                light = true;
                
                break;
              case ("TURN OFF"):
                   light = false;
                  break;
              case ("SWITCH"):
                  light = !light;
                  break;
              case ("TURN ODD"):
                  for (int i = 0,j=1; i < j; i++)
                  { Console.WriteLine("JAJAJJAJAJJAJAJJJAJAJJAJAJJAJAJJJAJAJJAJAJJAJAJJJAJAJJAJAJJAJAJJJAJAJJAJAJJAJAJJ");
                      j++;
                      if (j == 100000)
                      {
                          break;
                      }
                  }
                  Console.Clear();
                  break;

            }

            return light;
        }
/// <summary>
/// Here we print the comment to the action the user made.
/// </summary>
/// <param name="light">The light 👍 </param>
/// <param name="lights">The controler </param>
        private void mesage(bool light, bool lights)
        {
            if (light == lights)
            {
                if (light)
                {
                    Console.WriteLine("No pots tornar a encendre la llum");
                }
                else
                {
                    Console.WriteLine("La Llum ja està apagada!!!");
                }
            }
           
            if (light) { Console.WriteLine("La llum actualment està encesa"); }
             else { Console.WriteLine("La llum actualment està apagada"); }
                
            
        }
        
    }
    
}