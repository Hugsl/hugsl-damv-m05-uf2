using System;

namespace ParametritzacioMetodes
{ /// <summary>
    /// This is the menu page.... is a menu 👍 
    /// </summary>
    public class Menu
    {   /// <summary>
        /// This menu works with a char, while the char "a" is not 'e' this loops the menu again 
        /// </summary>
        public void Start()
        {   
            RIGHTTRIANGLESIZE rts = new RIGHTTRIANGLESIZE();
            LAMP l = new LAMP();
            CAMPSITEORGANIZER csor = new CAMPSITEORGANIZER();
            BASICROBOT br = new BASICROBOT();
            Threeinarow tir = new Threeinarow();
            Squashcounter sc = new Squashcounter();
            char a='0';
            do
            { Console.Clear();
                TEXTO();
                
                a=Console.ReadKey().KeyChar;
                switch (a)
                {
                    case '1':
                        rts.Start();
                        break;
                    case '2':
                        l.Start();
                        break;
                    case '3':
                        csor.Start();
                        break;
                    case '4':
                        br.Start();
                        break;
                    case '5':
                        tir.Start();
                        break;
                    case '6':
                        sc.Start();
                        break;
                }
                Console.Clear();
            } while (a!='e');

        }
     
/// <summary>
/// This is the menu's text, it's only the text 👍 
/// </summary>
        private void TEXTO()
        {
            Console.WriteLine("Se tiene que arreglar el 3!!!!");
            Console.WriteLine("MENU:");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("RIGHTTRIANGLESIZE_______1");
            Console.WriteLine("LAMP____________________2");
            Console.WriteLine("CAMPSITEORGANIZER_______3");
            Console.WriteLine("BASICROBOT______________4");
            Console.WriteLine("Threeinarow_____________5");
            Console.WriteLine("Squashcounter___________6");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("e to Exit");
            
        }
     
    }
}