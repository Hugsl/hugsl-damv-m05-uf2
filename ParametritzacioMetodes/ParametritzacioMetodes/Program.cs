﻿namespace ParametritzacioMetodes {
    /// <summary>
    ///In this namespace we do the exercises of parametrization.
    /// </summary>
    internal class Program
    { /// <summary>
      /// Only calls the menu
      /// </summary>
      private static void Main()
        {
            Menu a = new Menu(); a.Start();
        }
    } }