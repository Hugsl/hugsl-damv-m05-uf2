using System;

namespace ParametritzacioMetodes
{/// <summary>
 /// RightTriangleSize is a program that makes the calculations to obtain the  Hypotenuse ,Surface and perimeter from different rectangle triangles.
 /// </summary>
    public class RIGHTTRIANGLESIZE
    {
        /// <summary>
        /// Like the other programs, the start method is the "initialization" of the program 👍 
        /// </summary>
        public void Start() {
            // number of triangles the user wants to calculate.
        int tringnum = Convert.ToInt32(Console.ReadLine()); 
        double[] TrianglLenght= new double[tringnum];
        double[] TrianBase = new double[tringnum];
        double[] hipot =new double[tringnum] ;
        for (int i = 0; i < tringnum; i++)
        {
            TrianglLenght[i] = Convert.ToDouble(Console.ReadLine());
            TrianBase[i]= Convert.ToDouble(Console.ReadLine());
        }
        for (int i = 0; i < tringnum; i++)
        {
            hipot[i] = Math.Sqrt(Math.Pow(TrianglLenght[i],2 )+Math.Pow(TrianBase[i],2)); 
            Console.WriteLine($"La superfície del triangle és:{Superficie(hipot[i], TrianBase[i])} i el seu perímetre és de {Perimetre(TrianglLenght[i],TrianBase[i],hipot[i])}");
            
        }

        Console.ReadKey();
        }
        
        /// <summary>
        /// Here we calculate the surface 👍
        /// </summary>
        /// <param name="a">The Hypotenuse</param>
        /// <param name="b">The base of the triangle</param>
        /// <returns>Surface (Double)</returns>
        public static double Superficie(double a, double b)
        {
            var c = (a * b) / 2;
            return c;
        }
        /// <summary>
        /// Calculates the perimeter 👍
        /// </summary>
        /// <param name="a">Length</param>
        /// <param name="b">Base</param>
        /// <param name="c">Hypotenuse</param>
        /// <returns>The perimeter (Double)</returns>
        private double Perimetre(double a, double b, double c)
        {
            double d = a + b + c;
            return d;
        }
    }
}