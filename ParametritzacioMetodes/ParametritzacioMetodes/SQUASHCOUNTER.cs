/*
El exercici està malament fet que el contador de sets sigui al millor de X que has introduït  i que la f es que el partit s'anul·la
*/
using System;

namespace ParametritzacioMetodes
{/// <summary>
 /// This program calculates the score from a squash match, it's a BO5 and the referee can stop the match and the scores appear 👍
 /// </summary>
    public class Squashcounter
    {
        /// <summary>
        /// Like the rest, is the "initialization" of the program
        /// </summary>
        public void Start()
        {   Console.Clear();
            // P0,0 == points a || P0,1 == sets a || P1,0 == points b || P1,1 == sets b
            int[,] count = new int[,] {{0,0},{0,0}};
            int sets = Convert.ToInt32(Console.ReadLine());
            string all= "";
            char name = ' ';
            char name2 = ' ';
            do
            {
                while (name!='a' && name!='b' && name!='f')
                {
                    name = Console.ReadKey().KeyChar;
                }

                all += name;
                count = Count(count,name,name2);
                name2 = name;
                name = ' ';
            } while ((count[0,1]<sets/2 && count[1,1]<sets/2) || (name == 'f'));
            Console.Clear();
            Console.WriteLine(all);
            Console.Write("El resultat ha sigut:   ");
            Console.WriteLine(count[0,1]+ "  a  " + count[1,1] + "  sets");
            Console.ReadKey();
        }
/// <summary>
/// OK, here we count the the points and the sets.
/// </summary>
/// <param name="count">Te array of points</param>
/// <param name="name">the name of the scored point</param>
/// <param name="name2">the name of the second-to-last (and if is the same as the above it scores the point)</param>
/// <returns>The array of points</returns>
        public static int[,] Count(int[,] count, char name, char name2)
        {
            if (name == name2)
            {   Console.Clear();
                switch (name2)
                {
                    case 'a':
                        count[0, 0] += 1;   
                        break;
                    case 'b':
                        count[1, 0] += 1;
                        break;
                }
                if (count[0,0]>8 && count[0,0]-1>count[1,0])
                {
                    count[0, 0] = 0;
                    count[1, 0] = 0;
                    count[0, 1] += 1;
                }
                else if (count[1,0]>8 && count[0,0]-1>count[1,0])
                {   count[0, 0] = 0;
                    count[1, 0] = 0;
                    count[1, 1] += 1;
                }
                else
                {   
                    Console.WriteLine($"Van {count[0,0]} a {count[1,0]}");
                }
            }
            return count;
        }
    }
}
