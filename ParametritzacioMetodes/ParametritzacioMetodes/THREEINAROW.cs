using System;

namespace ParametritzacioMetodes
{
    /// <summary>
    /// Threeinarow the classic Tic Tac Toe 👍
    /// </summary>
    public class Threeinarow
    {/// <summary>
     /// Here we declare all the variables and is the initialization of the program
     /// </summary>
        public void Start()
        {
            Console.Clear();
            Console.WriteLine("You're in THREEINAROW");

            int winner;
            winner = Game();
            if (winner == 1)
            {
                Console.WriteLine("Player 'X' Wins");
            }
            else if (winner == 2)
            {
                Console.WriteLine("Player 'O' Wins");
            }
            else if (winner == 3)
            {
                Console.WriteLine("Tie");
            }
            else {
                for (int i = 0; i < 10000; i++)
                {
                    Console.WriteLine(" ERROR ");
                }

            }

            Console.ReadKey();


        }
/// <summary>
/// The Game starts and we ar ready to play, we set the location of the X or the O and whe put it in the board
/// </summary>
/// <returns>If it returns a 1, the player 1 won, and if it's a 2, the player 2 won and if it's a 3 it's a tie</returns>
        private int Game()
        {
            int winner = 0;
            char[,] taulell = new char[,]
            {
                {
                    '_', '_', '_'
                },
                {
                    '_', '_', '_'
                },
                {
                    '_', '_', '_'
                }
            };


            //0 == P1 | 1 == P2 
            int turn = 0;
            // char a == line | char b == column
            int a;
            int b;
            int count=0;
            do
            {
                if ( count>8)
                {
                    winner = 3;
                    return winner;
                }
                do
                {

                   
                    a = Seta();
                    b = Setb();
                    Console.Clear();
                    if (turn != 2)
                    {
                        if (taulell[a, b] == 'X' ||
                            taulell[a, b] == 'O')
                        {
                            Console.WriteLine("LLOC OCUPAT");
                        }
                        else if (turn == 0)
                        {
                            taulell[a, b] = 'X';
                            turn++;
                        }
                        else if (turn == 1)
                        {
                            taulell[a, b] = 'O';
                            turn--;
                        }
                       
                    }

                    
                } while (taulell[Convert.ToInt32(a), Convert.ToInt32(b)] == '_');

                Taulell(taulell);

                winner = CheckWinner(taulell, turn);
                
               
                
                count++;
            } while (winner == 0);

            return winner;
        }
/// <summary>
/// This is for print te board (id needs the array)
/// </summary>
/// <param name="taulell"></param>
        private void Taulell(char[,] taulell)
        {
            Console.WriteLine($"0 {taulell[0, 0]} | {taulell[0, 1]} | {taulell[0, 2]}");
            Console.WriteLine($"1 {taulell[1, 0]} | {taulell[1, 1]} | {taulell[1, 2]}");
            Console.WriteLine($"2 {taulell[2, 0]} | {taulell[2, 1]} | {taulell[2, 2]}");
        }
/// <summary>
/// chec the X in the board
/// </summary>
/// <returns>int</returns>
        private int Seta()
        {
            int a;
            do
            {
                a = Convert.ToInt32(Console.ReadLine());
            } while (a != 0 && a != 1 && a != 2);

            return a;
        }
/// <summary>
/// check the Y in the board
/// </summary>
/// <returns></returns>
        private int Setb()
        {
            int b;
            do
            {
                b = Convert.ToInt32(Console.ReadLine());
            } while (b != 0 && b != 1 && b != 2);

            return b;
        }
/// <summary>
/// Checks the winner, depend of the turn. That's it
/// </summary>
/// <param name="taulell">IS the array </param>
/// <param name="turn"></param>
/// <returns>int</returns>
        private int CheckWinner(char[,] taulell, int turn)
        {
            int winner;
            if (turn == 1)
            {
                winner = CheckWinnerX(taulell);
            }
            else
            {
                winner = CheckWinnerO(taulell);
               
            }

            return winner;
        }
/// <summary>
/// Checks if the winner is the X;
/// </summary>
/// <param name="taulell"></param>
/// <returns>If it wins or not (int)</returns>

        public static int CheckWinnerX(char[,] taulell)
        {
            int winner = 0;
            int winnerX = 0;

            for (int j = 0; j < 3 && winnerX < 3; j++)
            {
                for (int i = 0; i < taulell.GetLength(0) || winnerX == 3; i++)
                {
                    if (taulell[i, j] == 'X')
                    {
                        winnerX++;
                    }
                }

                if (winnerX < 3)
                {
                    winnerX = 0;
                    for (int i = 0; i < taulell.GetLength(1); i++)
                    {
                        if (taulell[j, i] == 'X')
                        {
                            winnerX++;
                        }
                    }

                    if (winnerX < 3)
                    {
                        winnerX = 0;
                        if ((taulell[0, 0] == 'X' && taulell[1, 1] == 'X' && taulell[2, 2] == 'X') ||
                            (taulell[0, 2] == 'X' && taulell[1, 1] == 'X' && taulell[2, 0] == 'X'))
                        {
                            winnerX = 3;
                        }

                    }

                }

            }


            if (winnerX == 3)
            {
                winner = 1;
            }

            return winner;
        }
/// <summary>
/// Checks if the winner is the O;
/// </summary>
/// <param name="taulell"></param>
/// <returns>If it wins or not (Int)</returns>
        private static int CheckWinnerO(char[,] taulell)
        {
            int winner = 0;
            int winnerO = 0;

            for (int j = 0; j < 3 && winnerO<3; j++)
            {
                for (int i = 0; i < taulell.GetLength(0) || winnerO == 3; i++)
                {
                    if (taulell[i, j] == 'O')
                    {
                        winnerO++;
                    }
                }

                if (winnerO <3)
                {
                    winnerO = 0;
                    for (int i = 0; i < taulell.GetLength(1); i++)
                    {
                        if (taulell[j, i] == 'O')
                        {
                            winnerO++;
                        }
                    }

                    if (winnerO < 3)
                    { winnerO = 0;
                        if ((taulell[0, 0] == 'O' && taulell[1, 1] == 'O' && taulell[2, 2] == 'O') ||
                            (taulell[0, 2] == 'O' && taulell[1, 1] == 'O' && taulell[2, 0] == 'O'))
                        {
                            winnerO = 3;
                        }
                      
                    }
                  
                }

            }
            if (winnerO == 3)
            {
                winner = 1;
            }
            return winner;
        }

    }
}