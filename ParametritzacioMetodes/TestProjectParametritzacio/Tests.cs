﻿using System;
using NUnit.Framework;
using ParametritzacioMetodes;

namespace TestProjectParametritzacio
{/// <summary>
 /// This is the test zone ( fun :|)
 /// </summary>
    [TestFixture]
    public class Tests
    {/// <summary>
     /// It checks if the user writes TURN ON it actually does that
     /// </summary>
        [Test]
        public void Light()
        {
            Assert.AreEqual(true,LAMP.Light("TURN ON",false));
        }
/// <summary>
/// It Checks the surface
/// </summary>
        [Test]
        public void Superficie()
        {
            Assert.AreEqual(6,RIGHTTRIANGLESIZE.Superficie(6,2));
        }
/// <summary>
/// CHecks if the X wins the Tic Tac toe match
/// </summary>
        [Test]
        public void CheckWinnerX()
        {
            Assert.AreEqual(1,Threeinarow.CheckWinnerX(  new char[,]
            {
                {'X', 'X', 'X'}, {'_', '_', '_'}, {'_', '_', '_'}}));
        }
/// <summary>
/// it checks if the set count works
/// </summary>
        [Test]
        public void Count()
        {
            Assert.AreEqual(new int[,]{{0,1},{0,0}}, Squashcounter.Count(new int[,] {{9, 0}, {0, 0}},'a','a'));
        }
        }
        
    }